class TestPage(TestCase):
	def test_home_page_works(self):
		response = self.client.get("/")
		response = self.client.get(reverse("about_us"))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'home.html')
		self.assertContains(response, 'BookTime')


from django.test import TestCase
from django.core import mail
from main import forms

class TestForm(TestCase):
	def test_valid_contact_us_form_sends_email(self):
    form = forms.ContactForm({
      'name': "Luke Skywalker",
      'message': "Hi there"})
    self.assertTrue(form.is_valid())
    with self.assertLogs('main.forms', level="INFO") as cm:
      form.send_mail()
    self.assertEqual(len(mail.outbox), 1)
    self.assertEqual(mail.outbox[0].subject, 'Site message')
    self.assertGreaterEqual(len(cm.output), 1)
  def test_invalid_contact_us_form(self):
    form = forms.ContactForm({
      'message': "Hi there"})
    self.assertFalse(form.is_valid())


from decimal import Decimal
from django.test import TestCase
from django.core.files.images import ImageFile
from main import models

class TestSignal(TestCase):
  def test_thumbnails_are_generated_on_save(self):
    product = models.Product(
      name="The cathedral and the bazaar",
      price=Decimal("10.00"),
    )
    product.save()
    with open(
      "main/fixtures/the-cathedral-the-bazaar.jpg", "rb"
    ) as f:
      image = models.ProductImage(
        product=product,
        image=ImageFile(f, name="tctb.jpg"),)
      with self.assertLogs("main", level="INFO") as cm:
        image.save()
    self.assertGreaterEqual(len(cm.output), 1)
    image.refresh_from_db()
    with open(
      "main/fixtures/the-cathedral-the-bazaar.thumb.jpg",
      "rb",
    ) as f:
      expected_content = f.read()
      assert image.thumbnail.read() == expected_content
    image.thumbnail.delete(save=False)
    image.image.delete(save=False)


class TestModel(TestCase):
	def test_active_manager_works(self):
		models.Product.objects.create(
			name="The cathedral and the bazaar",
			price=Decimal("10.00"),
			active=True)
		models.Product.objects.create(
			name="Pride and Prejudice",
			price=Decimal("2.00"),
			active=True)
		models.Product.objects.create(
			name="A Tale of Two Cities",
			price=Decimal("2.00"),
			active=False)
		self.assertEqual(len(models.Product.objects.active()), 2)